const gulp = require('gulp');
const sass = require('gulp-sass');
const server = require('browser-sync').create();

const paths = {
    scripts: {
        src: "js/**/*.js",
        dest: "js/",
    },
    styles: {
        src: "scss/**/*.scss",
        dest: "css/",
    }
}

function styles() {
    return gulp.src(paths.styles.src)
               .pipe(sass())
               .pipe(gulp.dest(paths.styles.dest));
}

function reload(done) {
    server.reload();
    done();
}

function serve(done) {
    server.init({
        files: ['./**/*.*'],
        server: {
            baseDir: './',
        },
        notify: false,
    });
    done();
}

const watch = () => gulp.watch(paths.styles.src, gulp.series(styles, reload));

const dev = gulp.series(styles, serve, watch);

gulp.task('default', dev);